import { Button } from '@mui/material'
import React from 'react'
import { IFeatures } from '../../@types/types'
import "./index.css"

export const FeatureSeaction:React.FC<IFeatures> = ({name, img_url}) => {
  return (
    <div className='featureContainer'>
      <div className='imgContainer'>
        <img src={img_url} alt="furniture_img"/>
      </div>
      <div className='titleContainer'>
        <h3>{name}</h3>
      </div>
      <div className='buttonContainer'>
        <Button variant="outlined">Learn More</Button>
      </div>
    </div>
  )
}
