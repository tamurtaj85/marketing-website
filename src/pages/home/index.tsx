import { Button } from '@mui/material';
import React from 'react'
import { FeatureSeaction } from '../../components';
import './index.css';

export const Home = () => {
  return (
   <div>
      {/* Title Container */}
      <div className='div-bg title'>
        <h1>Squared Marketing</h1>
      </div>
      {/* Basic Info Container */}
      <div className='info'>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Autem a consectetur suscipit mollitia incidunt quasi cupiditate impedit accusamus, rerum laboriosam optio error ea distinctio, praesentium dolorum nostrum beatae! Sed, voluptatum.
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Autem a consectetur suscipit mollitia incidunt quasi cupiditate impedit accusamus, rerum laboriosam optio error ea distinctio, praesentium dolorum nostrum beatae! Sed, voluptatum.
        </p>
      </div>
      {/* Feature Section */}
      <div className='feature'>
        <FeatureSeaction 
          name='Straight lines and edged furniture' 
          img_url='https://linesmag.com/wp-content/uploads/2019/07/Straight-lines-and-edges-Ruda-Studio.jpg'
        />
        <FeatureSeaction 
          name='Edged modern sofa' 
          img_url='https://linesmag.com/wp-content/uploads/2019/07/deavita.net_.jpg'
        />
        <FeatureSeaction 
          name='Gwen Sofa' 
          img_url='https://linesmag.com/wp-content/uploads/2019/07/Gwen-Sofa-by-Jeremiah-Brent.jpg'
        />
      </div>
      {/* Some Random text */}
      <div className='callToAction'>
         <p>step into luxary</p> 
      </div>
      {/* Contact Us */}
      <div className='contact'>
        <Button variant="contained" size='large' color='inherit'>Contact Us</Button>
      </div>
   </div>
  )
}
